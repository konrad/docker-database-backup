# Database Backup

> A simple script to backup databases from multiple hosts at once.

This script backups in a configurable schedule all databases on multiple available hosts.
It was built to run in docker and have a clean way to backup database containers.

## Usage

1. Create a configuration file with all hosts and credentials you want to backup
2. Point the script to it (See environment variables below). By default, it searches in the default directory for a file called `backup_hosts.json`.

## Configuration

A configuration file with all defaults looks like this:

```json
[
   {
      "host":"localhost",
      "user":"root",
      "password":"",
      "port": 3306
   }
]
```

You can omit any field to you use the default value.

For example, a configuration which backups a db on `localhost` with the `root` user and `password` `1234` on the default port `3306` would look like this:

```json
[
   {
      "password":"1234",
   }
]
```

## Usage with docker

A docker image with the script is available, to integrate nicely with existing docker infrastructure.

**Note:** You need to make sure that all database containers can be reached by the script over network.
The easiest way to achieve this is to create an external network and put the backup container and the database container in it.

### Docker Compose Example

This will mount the config from the `config` and `backup` folders in the same directory.

```yaml
version: '2'
services:
  backup:
    image: kolaente/database-backup
    restart: unless-stopped
    environment:
      CRON_TIME: 0 */3 * * *
      DB_BACKUP_MAX: '36'
    volumes:
      - ./config:/config
      - ./backups:/backups
      - /etc/localtime:/etc/localtime:ro
    networks:
      - default
      - backup

networks:
  backup:
    external: true
```

#### Environment variables

* `CRON_TIME`: A cron formatted string about how often the script should run
* `DB_BACKUP_MAX`: How many old backups to keep before cleanup
* `DB_BACKUP_HOSTS_FILE`: Where the backup configuration file is stored
* `DB_BACKUP_FOLDER`: Where backups should be stored

# License

(c) 2018-2020 K. Langenberg. Usage of this software is guarded by the GPLv3 which can be found in this repo.

